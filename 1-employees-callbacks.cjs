/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require('fs')
const path = require('path')

const filename = path.join(__dirname, 'data.json')

function callbackFn(err, data) {
    if (err) {
        console.error(err)
    } else {
        console.log(data)
    }
}

function employeesCallbacks(callbackFn) {

    fs.readFile(filename, 'utf-8', (err, data) => {
        // 1. Retrieve data for ids : [2, 13, 23].
        if (err) {
            callbackFn(err)
        } else {
            console.log(`data.json has been read successfully`)

            const arrayWithIds = [2, 13, 23]
            const employeesWithIds = JSON.parse(data).employees.filter((employee) => {
                return arrayWithIds.includes(employee.id)
            })

            fs.writeFile(path.resolve(__dirname, 'dataWithIds.json'), JSON.stringify(employeesWithIds), (err) => {
                if (err) {
                    callbackFn(err)
                } else {
                    console.log(`dataWithIds.json has been written successfully`)

                    fs.readFile(filename, 'utf-8', (err, data) => {
                        // 2. Group data based on companies.
                        //     { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
                        if (err) {
                            callbackFn(err)
                        } else {
                            console.log(`data.json has been read successfully`)

                            const groupedEmployees = JSON.parse(data).employees.reduce((accu, employee) => {
                                if (accu[employee.company] === undefined) {
                                    accu[employee.company] = []
                                }
                                accu[employee.company].push(employee)
                                return accu
                            }, {})

                            fs.writeFile(path.resolve(__dirname, 'groupByCompanies.json'), JSON.stringify(groupedEmployees), (err) => {
                                if (err) {
                                    callbackFn(err)
                                } else {
                                    console.log(`groupByCompanies.json has been written successfully`)

                                    fs.readFile(filename, 'utf-8', (err, data) => {
                                        // 3. Get all data for company Powerpuff Brigade
                                        if (err) {
                                            callbackFn(err)
                                        } else {
                                            console.log(`data.json has been read successfully`)

                                            const powerpuffEmployees = JSON.parse(data).employees.filter((employee) => {
                                                return employee.company === 'Powerpuff Brigade'
                                            })

                                            fs.writeFile(path.resolve(__dirname, 'dataByCompany.json'), JSON.stringify({ powerpuffEmployees }), (err) => {
                                                if (err) {
                                                    callbackFn(err)
                                                } else {
                                                    console.log(`dataByCompany.json has been written successfully`)

                                                    fs.readFile(filename, 'utf-8', (err, data) => {
                                                        // 4. Remove entry with id 2.
                                                        if (err) {
                                                            callbackFn(err)
                                                        } else {
                                                            console.log(`data.json has been read successfully`)

                                                            const employeesWithoutId2 = JSON.parse(data).employees.filter((employee) => {
                                                                return employee.id !== 2
                                                            })
                                                            fs.writeFile(path.resolve(__dirname, 'removeIdWith.json'), JSON.stringify({ employeesWithoutId2 }), (err) => {
                                                                if (err) {
                                                                    callbackFn(err)
                                                                } else {
                                                                    console.log(`removeIdWith.json has been written successfully`)

                                                                    fs.readFile(filename, 'utf-8', (err, data) => {
                                                                        // 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
                                                                        if (err) {
                                                                            callbackFn(err)
                                                                        } else {
                                                                            console.log(`data.json has been read successfully`)

                                                                            const sortedEmployees = JSON.parse(data).employees.sort((employee1, employee2) => {
                                                                                if (employee1.company > employee2.company) {
                                                                                    return 1
                                                                                } else if (employee1.company === employee2.company) {
                                                                                    return employee1.id - employee2.id
                                                                                }
                                                                                return -1
                                                                            })

                                                                            fs.writeFile(path.resolve(__dirname, 'sortByCompany.json'), JSON.stringify({ sortedEmployees }), (err) => {
                                                                                if (err) {
                                                                                    callbackFn(err)
                                                                                } else {
                                                                                    console.log(`sortByCompany.json has been written successfully`)

                                                                                    fs.readFile(filename, 'utf-8', (err, data) => {
                                                                                        // 6. Swap position of companies with id 93 and id 92.
                                                                                        if (err) {
                                                                                            callbackFn(err)
                                                                                        } else {
                                                                                            console.log(`data.json has been read successfully`)

                                                                                            const swappedEmployees = JSON.parse(data).employees
                                                                                            const employeeId1 = swappedEmployees.find((employee) => {
                                                                                                return employee.id === 93
                                                                                            })

                                                                                            const employeeId2 = swappedEmployees.find((employee) => {
                                                                                                return employee.id === 94
                                                                                            })
                                                                                            const index1 = swappedEmployees.indexOf(employeeId1)
                                                                                            const index2 = swappedEmployees.indexOf(employeeId2)
                                                                                            swappedEmployees[index1] = employeeId2
                                                                                            swappedEmployees[index2] = employeeId1

                                                                                            fs.writeFile(path.resolve(__dirname, 'swap93and94.json'), JSON.stringify({ swappedEmployees }), (err) => {
                                                                                                if (err) {
                                                                                                    callbackFn(err)
                                                                                                } else {
                                                                                                    console.log(`swap93and94.json has been written successfully`)

                                                                                                    fs.readFile(filename, 'utf-8', (err, data) => {
                                                                                                        // 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
                                                                                                        if (err) {
                                                                                                            callbackFn(err)
                                                                                                        } else {
                                                                                                            console.log(`data.json has been read successfully`)

                                                                                                            const employeeWithBirthdays = JSON.parse(data).employees.map((employee) => {
                                                                                                                if (employee.id % 2 === 0) {
                                                                                                                    employee['birthday'] = new Date()
                                                                                                                }
                                                                                                                return employee
                                                                                                            })

                                                                                                            fs.writeFile(path.resolve(__dirname, 'addBirthday.json'), JSON.stringify({ employeeWithBirthdays }), (err) => {
                                                                                                                if (err) {
                                                                                                                    callbackFn(err)
                                                                                                                } else {
                                                                                                                    console.log(`addBirthday.json has been written successfully`)

                                                                                                                    callbackFn(null, "all data has been written successfully")
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

employeesCallbacks(callbackFn)